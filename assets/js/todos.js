
function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

function toggleClass(obj, className){

	if (obj.className  == className){
		obj.className = ""; }
	
	else if (obj.className == ""){
		obj.className = className;
	}
}

function hasParentOfType(obj, tagName){
	while (obj && (obj != document)){
		if (obj.nodeName === 'SPAN'){	
			return obj;
		}
		obj = obj.parentElement;
	}
	return false;
}

ready(function() {

		// Check Off ToDo Item By Clicking
	 	document.addEventListener('click',function(e){
		    if(e.target && e.target.tagName === 'LI'){
		        toggleClass(e.target, "completed");
		    	//alert('clicked');
	 		}	

 		})

	 	
	 	// Check Off ToDo Item By Clicking
	 	document.addEventListener('click',function(e){
	 		
	 		spanObj = hasParentOfType(e.target, 'SPAN');
		    if(spanObj){
		    	fadeOut(spanObj.parentNode, 200, function (){
		    		spanObj.parentNode.parentNode.removeChild(spanObj.parentNode);
		    	});
		    	
	 		}	

 		})

      // Add to Item

    var inputBox = document.querySelector("input[type='text']");
    inputBox.addEventListener('keydown', function(e){
    if(e.which === 13){
            // Get text from input
            var todoText = e.target.value;
            e.target.value = "";
            // Create new LI and add to ul
            var parentNode  = document.querySelector("ul");
            var newLI       = document.createElement("li");
            newLI.innerHTML = "<span> <i class='fa fa-trash'></i> </span>" + todoText;
            document.querySelector("ul").appendChild(newLI);
        }
      }
    )
    
    var plusButton = document.querySelector(".fa-plus");
    plusButton.addEventListener('click', function(e){
        var inputBox = document.querySelector("input[type='text']");
        if (window.getComputedStyle(inputBox).visibility == "visible"){
        fadeOut(inputBox, 100, function(){});}
        else{
          fadeIn(inputBox, 100, function(){});
        }
    })

    /*
    keypress(function(e){
    if(e.which === 13){
      // Get text from input
      var todoText = $(this).val();
      $(this).val("");
      // Create new LI and add to ul
      $("ul").append("<li><span> <i class='fa fa-trash'></i> </span>" + todoText + "</li>")
    }
    */
      
  });

	 /*
	$("ul").on("click", "span", function(e){
		$(this).parent().fadeOut(500,function(){
				this.remove();
			});
		e.stopPropagation();
	})

	// Add to Item
	$("input[type='text']").keypress(function(e){
		if(e.which === 13){
			// Get text from input
			var todoText = $(this).val();
			$(this).val("");
			// Create new LI and add to ul
			$("ul").append("<li><span> <i class='fa fa-trash'></i> </span>" + todoText + "</li>")
		}
			
	});

	$(".fa-plus").click(function(){
		$("input[type='text']").fadeToggle(100);
	})

	*/



function fadeIn( elem, ms )
{
  if( ! elem )
    return;

  elem.style.opacity = 0;
  elem.style.filter = "alpha(opacity=0)";
  elem.style.display = "inline-block";
  elem.style.visibility = "visible";

  if( ms )
  {
    var opacity = 0;
    var timer = setInterval( function() {
      opacity += 50 / ms;
      if( opacity >= 1 )
      {
        clearInterval(timer);
        opacity = 1;
      }
      elem.style.opacity = opacity;
      elem.style.filter = "alpha(opacity=" + opacity * 100 + ")";
    }, 50 );
  }
  else
  {
    elem.style.opacity = 1;
    elem.style.filter = "alpha(opacity=1)";
  }
}

function fadeOut( elem, ms, callback)
{
  if( ! elem )
    return;

  if( ms )
  {
    var opacity = 1;
    var timer = setInterval( function() {
      opacity -= 50 / ms;
      if( opacity <= 0 )
      {
        clearInterval(timer);
        opacity = 0;
        elem.style.display = "none";
        elem.style.visibility = "hidden";
        callback();
      }
      elem.style.opacity = opacity;
      elem.style.filter = "alpha(opacity=" + opacity * 100 + ")";
    }, 50 );
  }
  else
  {
    elem.style.opacity = 0;
    elem.style.filter = "alpha(opacity=0)";
    elem.style.display = "none";
    elem.style.visibility = "hidden";
    callback();
    
  }

}